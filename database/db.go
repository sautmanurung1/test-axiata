package database

import (
	"database/sql"
	"fmt"

	"log"
	"os"

	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
)

func ConnectDB() (*sql.DB, error) {
	err := godotenv.Load()
	if err != nil {
		log.Printf("Warning: Failed to load environment variable from .env file: %v", err)
	}

	dbUser := os.Getenv("DB_USER")
	dbPassword := os.Getenv("DB_PASSWORD")
	dbName := os.Getenv("DB_NAME")
	dbHost := os.Getenv("DB_HOST")
	dbPort := os.Getenv("DB_PORT")

	connStr := fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=disable", dbHost, dbPort, dbUser, dbName, dbPassword)
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		return nil, err
	}

	return db, nil
}

func CreateTables(db *sql.DB) {
	// Create the "posts" table
	_, err := db.Exec(`
		CREATE TABLE IF NOT EXISTS posts (
			id SERIAL PRIMARY KEY,
			title TEXT NOT NULL,
			content TEXT NOT NULL,
			status TEXT NOT NULL,
			publish_date TEXT
		)
	`)

	if err != nil {
		log.Fatalf("Failed to create posts table: %v", err)
	}

	// Create the "tags" table
	_, err = db.Exec(`
		CREATE TABLE IF NOT EXISTS tags (
			id SERIAL PRIMARY KEY,
			label TEXT UNIQUE NOT NULL
		)
	`)

	if err != nil {
		log.Fatalf("Failed to create tags table: %v", err)
	}

	// Create the "post_tags" table (many-to-many relationship)
	_, err = db.Exec(`
		CREATE TABLE IF NOT EXISTS post_tags (
			post_id INTEGER REFERENCES posts(id) ON DELETE CASCADE,
			tag_id INTEGER REFERENCES tags(id) ON DELETE CASCADE,
			PRIMARY KEY (post_id, tag_id)
		)
	`)

	if err != nil {
		log.Fatalf("Failed to create post_tags table: %v", err)
	}
}
