package routes

import (
	"database/sql"
	"net/http"
	"test-axiata/controllers"
)

func SetupRoutes(db *sql.DB) *http.ServeMux {
	router := http.NewServeMux()

	postController := &controllers.PostController{DB: db}
	router.HandleFunc("/api/posts", postController.HandlePosts)
	router.HandleFunc("/api/posts/", postController.HandlePostByID)

	return router
}
