package main

import (
	"log"
	"net/http"
	"test-axiata/database"
	"test-axiata/routes"
)

func main() {
	db, err := database.ConnectDB()

	if err != nil {
		log.Fatalf("Failed to connect to the database: %v", err)
	}

	defer db.Close()

	database.CreateTables(db)

	router := routes.SetupRoutes(db)

	log.Println("Starting server on :8080")
	log.Fatal(http.ListenAndServe(":8080", router))
}
