package controllers

import (
	"database/sql"
	"encoding/json"
	"net/http"
	"strconv"
	"strings"
	"test-axiata/models"

	"github.com/lib/pq"
)

type PostController struct {
	DB *sql.DB
}

func (pc *PostController) HandlePosts(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		// Check if the request contains a tag parameter
		if tag := r.URL.Query().Get("tag"); tag != "" {
			pc.SearchPostsByTag(w, r)
		} else {
			pc.getPosts(w, r)
		}
	case http.MethodPost:
		pc.createPost(w, r)
	default:
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
	}
}

func (pc *PostController) HandlePostByID(w http.ResponseWriter, r *http.Request) {
	parts := strings.Split(r.URL.Path, "/")
	if len(parts) != 4 {
		http.Error(w, "Invalid URL path", http.StatusBadRequest)
		return
	}

	postID, err := strconv.Atoi(parts[3])
	if err != nil {
		http.Error(w, "Invalid post ID", http.StatusBadRequest)
		return
	}

	switch r.Method {
	case http.MethodGet:
		pc.getPost(w, r, postID)
	case http.MethodPut:
		pc.updatePost(w, r, postID)
	case http.MethodDelete:
		pc.deletePost(w, r, postID)
	default:
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
	}
}

func (pc *PostController) SearchPostsByTag(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	tagLabel := r.URL.Query().Get("tag")
	if tagLabel == "" {
		pc.getPosts(w, r)
		return
	}

	// SQL query to retrieve posts with the specified tag
	rows, err := pc.DB.Query(`
        SELECT p.id, p.title, p.content, p.status, p.publish_date, COALESCE(array_agg(t.label), '{}') AS tags
        FROM posts p
        LEFT JOIN post_tags pt ON p.id = pt.post_id
        LEFT JOIN tags t ON pt.tag_id = t.id
        WHERE t.label = $1
        GROUP BY p.id
    `, tagLabel)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer rows.Close()

	var posts []models.Post
	for rows.Next() {
		var post models.Post
		var tagLabels string
		err := rows.Scan(&post.ID, &post.Title, &post.Content, &post.Status, &post.PublishDate, &tagLabels)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		tagStrings := strings.Split(tagLabels, ",")
		uniqueTags := make(map[string]bool)
		for _, tag := range tagStrings {
			tag = strings.TrimSpace(tag)
			if tag != "" && !uniqueTags[tag] {
				uniqueTags[tag] = true
				post.Tags = append(post.Tags, models.Tag{Label: tag})
			}
		}

		posts = append(posts, post)
	}

	if err = rows.Err(); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(posts)
}

func (pc *PostController) getPosts(w http.ResponseWriter, r *http.Request) {
	rows, err := pc.DB.Query("SELECT p.id, p.title, p.content, p.status, p.publish_date, array_agg(t.id), array_agg(t.label) AS tags FROM posts p LEFT JOIN post_tags pt ON p.id = pt.post_id LEFT JOIN tags t ON pt.tag_id = t.id GROUP BY p.id")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer rows.Close()

	var posts []models.Post
	for rows.Next() {
		var post models.Post
		var tagIDs []sql.NullString
		var tagLabels []string
		err := rows.Scan(&post.ID, &post.Title, &post.Content, &post.Status, &post.PublishDate, pq.Array(&tagIDs), pq.Array(&tagLabels))
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		for i, label := range tagLabels {
			if i < len(tagIDs) && tagIDs[i].Valid {
				id, _ := strconv.Atoi(tagIDs[i].String)
				post.Tags = append(post.Tags, models.Tag{ID: id, Label: label})
			}
		}

		posts = append(posts, post)
	}

	if err = rows.Err(); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(posts)
}

func (pc *PostController) getPost(w http.ResponseWriter, r *http.Request, postID int) {
	row := pc.DB.QueryRow("SELECT p.id, p.title, p.content, p.status, p.publish_date, array_agg(t.label) AS tags FROM posts p LEFT JOIN post_tags pt ON p.id = pt.post_id LEFT JOIN tags t ON pt.tag_id = t.id WHERE p.id = $1 GROUP BY p.id", postID)

	var post models.Post
	var tagIDs, tagLabels []sql.NullString
	err := row.Scan(&post.ID, &post.Title, &post.Content, &post.Status, &post.PublishDate, pq.Array(&tagLabels))
	if err != nil {
		if err == sql.ErrNoRows {
			http.Error(w, "Post not found", http.StatusNotFound)
			return
		}
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	for i, label := range tagLabels {
		if label.Valid {
			id, _ := strconv.Atoi(tagIDs[i].String)
			post.Tags = append(post.Tags, models.Tag{ID: id, Label: label.String})
		}
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(post)
}

func (pc *PostController) createPost(w http.ResponseWriter, r *http.Request) {
	var post models.Post
	err := json.NewDecoder(r.Body).Decode(&post)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	tx, err := pc.DB.Begin()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	defer tx.Rollback()

	err = tx.QueryRow(`
		INSERT INTO posts (title, content, status, publish_date)
		VALUES ($1, $2, $3, $4)
		RETURNING id
	`, post.Title, post.Content, "draft", post.PublishDate).Scan(&post.ID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	for _, tag := range post.Tags {
		var tagID int
		err = tx.QueryRow(`
			INSERT INTO tags (label)
			VALUES ($1)
			ON CONFLICT (label) DO UPDATE SET label = excluded.label
			RETURNING id
		`, tag.Label).Scan(&tagID)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		_, err = tx.Exec(`
			INSERT INTO post_tags (post_id, tag_id)
			VALUES ($1, $2)
		`, post.ID, tagID)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}

	err = tx.Commit()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(post)
}

func (pc *PostController) updatePost(w http.ResponseWriter, r *http.Request, postID int) {
	var post models.Post
	err := json.NewDecoder(r.Body).Decode(&post)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	post.ID = postID

	tx, err := pc.DB.Begin()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer tx.Rollback()

	_, err = tx.Exec(`
        UPDATE posts
        SET title = $1, content = $2, status = $3, publish_date = $4
        WHERE id = $5
    `, post.Title, post.Content, post.Status, post.PublishDate, post.ID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	_, err = tx.Exec(`
        DELETE FROM post_tags
        WHERE post_id = $1
    `, post.ID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var tagLabels []string
	for _, tag := range post.Tags {
		tagLabels = append(tagLabels, tag.Label) // Assuming the tag label is stored in the "Label" field
	}

	for _, tagLabel := range tagLabels {
		var tagID int
		// Insert or update the tag
		err = tx.QueryRow(`
            INSERT INTO tags (label)
            VALUES ($1)
            ON CONFLICT (label) DO UPDATE SET label = excluded.label
            RETURNING id
        `, tagLabel).Scan(&tagID)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		// Insert post-tag relationship
		_, err = tx.Exec(`
            INSERT INTO post_tags (post_id, tag_id)
            VALUES ($1, $2)
        `, post.ID, tagID)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}

	err = tx.Commit()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(post)
}

func (pc *PostController) deletePost(w http.ResponseWriter, r *http.Request, postID int) {
	_, err := pc.DB.Exec(`
		DELETE FROM posts
		WHERE id = $1
	`, postID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}
