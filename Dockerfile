FROM golang:alpine

# Create a non-root user
RUN adduser -D appuser

# Switch to the non-root user
USER appuser

# Set the working directory
WORKDIR /app

# Copy the source code
COPY . .

# Install any required Go dependencies
RUN go mod download

# Copy the source code
COPY . .

# Expose the port your application listens on
EXPOSE 8080

# Start the application
CMD ["go", "run", "main.go"]