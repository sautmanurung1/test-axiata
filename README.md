# API BLOG SISTEM

Tech stack :
- Golang
- godotenv (library for read .env file)
- lib/pq (library for database)


# How to use this code in your local env if you not use docker

- first, you must install that library/depedencies you can use this command to install your library/depedencies
```
go mod download
```

- after your library/depedencies installed in your local, you can do this to insert env file
```
copy .env.example in to your .env file
```

- after you make .env file in your local, you can running that code using this command in your terminal
```
go run main.go
```

# How to use this code in your local env if you use docker compose and docker desktop
- first, you must install docker in your local
- after you docker installed in your local and your .env file inserted in your local, you can running this command to build your docker container
```
docker-compose up -d --build
```
- after your docker container running, you can test that api in your local

# How to test this API
- Get posts by id (*GET*)
```
{host_url}/api/posts/{post_id}
```

- Get all posts (*GET*)
```
{host_url}/api/posts
```

- Get post from tag posts (*GET*)
```
{host_url}/api/posts/?tag={label}
```


- Create posts (*POST*)
```
{host_url}/api/posts
```

in crate post you can added this body request
```
{
"title": "REST API with Go",
"content": "Lorem ipsum dolor amet ganteng",
 "tags": [
    {"label": "Golang"},
    {"label": "API Rest"}
  ],
  "publish_date":"2024-01-12"
}
```

- Update Posts by id (*PUT*)
```
{host_url}/api/posts/{post_id}
```

in update post you can added this body request but you must know the post id if you want to update the data
```
{
"title": "REST API with Go",
"content": "Lorem ipsum dolor amet ganteng",
 "tags": [
    {"label": "Golang"},
    {"label": "API Rest"}
  ],
  "publish_date":"2024-01-12"
}
```

- Delete posts by id (*DELETE*)
```
{host_url}/api/posts/{post_id}
```

## you can change {host_url} using ```localhost:your port local``` if you don't know how to make environment in your postman